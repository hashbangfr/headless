from pathlib import Path

import pytest
from alembic import command as alembic_command
from alembic import config as alembic_config
from pytest_factoryboy import register

from headless.application import app as headless_app
from headless.application import configure_app, db
from headless.factories import ArticleFactory, AuthorFactory

register(ArticleFactory)
register(AuthorFactory)


@pytest.fixture(scope="session")
def app(alembic):
    configure_app(headless_app)
    headless_app.config["TESTING"] = True
    return headless_app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.yield_fixture(scope='session', autouse=True)
def alembic():
    cfg = alembic_config.Config(Path(__file__).parent / ".." / 'alembic.ini')
    with db.engine.begin() as connection:
        cfg.attributes['connection'] = connection
        alembic_command.upgrade(cfg, "head")
        yield None
