from flask import request
from flask_restful import Resource, reqparse
from sqlalchemy_filter import Filter, fields

from .application import db
from .models import Article
from .schemas import ArticleSchema


class ArticleFilter(Filter):
    title_ilike = fields.Field(field_name="title", lookup_type="ilike")

    class Meta:
        model = Article


class ArticleListResource(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('page', type=int, default=1)
        parser.add_argument('per_page', type=int, default=10)
        parser.add_argument('title_ilike')
        args = parser.parse_args()

        article_pagination = (
            ArticleFilter()
            .filter_query(Article.query, request.args)
            .paginate(args.page, args.per_page)
        )
        articles_schema = ArticleSchema(
            many=True, exclude=['tag_ids', 'author_id']
        )

        headers = {
            "x-next-page": article_pagination.next_num or "",
            "x-per-page": args.per_page,
            "x-page": args.page,
            "x-prev-page": article_pagination.prev_num or "",
            "x-total": article_pagination.total,
        }

        return articles_schema.dump(article_pagination.items), 200, headers

    def post(self):
        article_schema = ArticleSchema(exclude=['tags', 'author'])
        article = article_schema.load(request.json, session=db.session)
        db.session.add(article)
        db.session.commit()
        return article_schema.dump(article)


class ArticleResource(Resource):
    def get(self, article_id):
        article = db.session.query(Article).get(article_id)
        article_schema = ArticleSchema(exclude=['author', 'tags'])
        return article_schema.dump(article)

    def put(self, article_id):
        article_schema = ArticleSchema(exclude=['tags', 'author'])
        article = article_schema.load(request.json, session=db.session)
        db.session.add(article)
        db.session.commit()
        return article_schema.dump(article)

    def delete(self, article_id):
        article = db.session.query(Article).get(article_id)
        db.session.delete(article)
        db.session.commit()
        return '', 204
