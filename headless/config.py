import os
from pathlib import Path

DB_URL = os.environ.get(
    "HEADLESS_DB_URL",
    "sqlite:///" + str(Path(__file__) / ".." / ".." / "db.sqlite"),
)
