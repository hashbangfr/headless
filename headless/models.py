from datetime import datetime

from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Table,
    Text,
)
from sqlalchemy.orm import relationship

from .application import db

article_tags = Table(
    'article_tags',
    db.Model.metadata,
    Column('article_id', ForeignKey('articles.id'), primary_key=True),
    Column('tag_id', ForeignKey('tags.id'), primary_key=True),
)


class Tag(db.Model):
    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))

    articles = relationship(
        'Article', secondary=article_tags, back_populates='tags'
    )

    def __repr__(self):
        return '<Tag {}: {}>'.format(self.id, self.name)


class Author(db.Model):
    __tablename__ = 'authors'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    email = Column(String(255), unique=True)

    articles = relationship(
        "Article",
        back_populates='author',
        cascade="all, delete, delete-orphan",
    )

    def __repr__(self):
        return '<Tag {}: {}>'.format(self.id, self.name)


class Article(db.Model):
    __tablename__ = 'articles'

    id = Column(Integer, primary_key=True)
    title = Column(String(255))
    content = Column(Text)
    created_at = Column(DateTime, default=datetime.now)

    author_id = Column(Integer, ForeignKey('authors.id'))
    author = relationship("Author", back_populates="articles")

    tags = relationship(
        'Tag', secondary=article_tags, back_populates='articles'
    )

    def __repr__(self):
        return '<Article {}: {}>'.format(self.id, self.title)
