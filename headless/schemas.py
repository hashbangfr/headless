from marshmallow import fields
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

from .application import db, ma
from .models import Article, Author, Tag


class AuthorSchema(SQLAlchemySchema):
    id = auto_field()
    name = auto_field()
    email = auto_field()

    class Meta:
        model = Author
        sqla_session = db.session


class TagSchema(SQLAlchemySchema):
    id = auto_field()
    name = auto_field()

    class Meta:
        model = Tag
        sqla_session = db.session


class ArticleSchema(SQLAlchemySchema):
    id = auto_field()
    title = auto_field()
    content = auto_field()

    author_id = auto_field()
    tag_ids = fields.Pluck(TagSchema, 'id', many=True, attribute='tags')

    tags = ma.Nested(TagSchema(many=True), dump_only=True)
    author = ma.Nested(AuthorSchema, dump_only=True)

    class Meta:
        model = Article
        sqla_session = db.session
        load_instance = True
