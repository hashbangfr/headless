import requests
from flask import render_template
from sqlalchemy.orm import joinedload

from headless.application import app, db
from headless.models import Article, Author, Tag


@app.route("/")
def hello_world():
    return "Hello, World!", 200, {"Content-Type": "text/plain; charset=utf-8"}


@app.route('/articles')
def article_list():
    articles = db.session.query(Article).all()
    return render_template('article/list.html', articles=articles)


@app.route('/articles/<int:article_id>')
def article_detail(article_id):
    article = db.session.query(Article).get(article_id)
    # article = db.session.query(Article).options(joinedload('tags'), joinedload('author')).get(article_id)
    if not article:
        return 'This article does not exist', 404
    return render_template('article/detail.html', article=article)


@app.route('/get_hashbang')
def get_hashbang():
    try:
        response = requests.get('https://hashbang.coop')
        return str(response.status_code)
    except ConnectionError:
        return "ConnectionError", 502
