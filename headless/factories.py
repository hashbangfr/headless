import factory
from factory.alchemy import SQLAlchemyModelFactory

from headless.models import Article, Author

from .application import db


class AuthorFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Author
        sqlalchemy_session = db.session
        force_flush = True

    name = factory.Sequence(lambda n: "User {}".format(n))
    email = factory.Sequence(lambda n: "user{}@headless.test".format(n))


class ArticleFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Article
        sqlalchemy_session = db.session
        force_flush = True

    title = factory.Sequence(lambda n: "Article title {}".format(n))
    content = factory.Sequence(lambda n: "Article content {}".format(n))
    author = factory.SubFactory(AuthorFactory,)
