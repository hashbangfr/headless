from unittest import mock

import pytest


def test_home(client):
    response = client.get("/")
    assert response.status_code == 200
    assert response.data == b"Hello, World!"
    assert response.headers["Content-Type"] == "text/plain; charset=utf-8"


def test_article_list_no_articles(client):
    response = client.get('/articles')
    assert response.status_code == 200
    body = response.data.decode()
    assert 'No articles' in body


def test_article_detail_404(client):
    response = client.get('/articles/0')
    assert response.status_code == 404
    body = response.data.decode()
    assert 'This article does not exist' in body


def test_article_list_with_one(client, article):
    response = client.get('/articles')
    assert response.status_code == 200
    body = response.data.decode()
    assert '/articles/{}'.format(article.id) in body


def test_article_detail_with_one(client, article):
    response = client.get('/articles/{}'.format(article.id))
    assert response.status_code == 200
    body = response.data.decode()
    assert article.title in body
    assert article.content in body


@pytest.mark.vcr()
def test_get_hashbang_vcr(client):
    response = client.get('/get_hashbang')
    assert response.status_code == 200
    assert response.data == b'200'


def test_get_hashbang_mock(client):
    with mock.patch('headless.views.requests.get') as get:
        get.side_effect = ConnectionError
        response = client.get('/get_hashbang')
        assert response.status_code == 502
        get.assert_called_with('https://hashbang.coop')
