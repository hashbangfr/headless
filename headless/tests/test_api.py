def test_api_articles_list(client, article):
    response = client.get('/api/articles')
    data = response.json

    author = article.author
    assert data == [
        {
            'tags': [],
            'author': {
                'name': author.name,
                'email': author.email,
                'id': author.id,
            },
            'title': article.title,
            'content': article.content,
            'id': article.id,
        }
    ]
