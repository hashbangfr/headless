from pathlib import Path

from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
from flask_marshmallow import Marshmallow
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from .config import DB_URL

app = Flask(
    "headless server", template_folder=Path(__file__).parent / 'templates'
)
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
db = SQLAlchemy(app)
api = Api(app)
ma = Marshmallow(app)


def load_endpoints():
    from .api import ArticleListResource, ArticleResource

    api.add_resource(
        ArticleListResource(), '/api/articles', endpoint="api_article_list"
    )
    api.add_resource(
        ArticleResource(),
        "/api/articles/<int:article_id>",
        endpoint="api_article_detail",
    )

    from headless import views  # noqa


def configure_app(app, debug=False):
    app.debug = debug
    app.config['SECRET_KEY'] = "rSYaF0AoJtE8wdw3S6CdodNAbaMqtHBGMkAtcePW"
    load_endpoints()
    DebugToolbarExtension(app)
