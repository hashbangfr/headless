from headless.application import app as application
from headless.application import configure_app

if __name__ == "__main__":
    configure_app(application, debug=True)
    application.run()
