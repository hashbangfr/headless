Headless
========

A demo project with Flask, SqlAlchemy, flask-restfull and marshmallow

Quickstart
----------

.. code-block:: bash

  # to install dependencies
  pip install -r requirements.txt -r requirements-tests.txt -e .

  # to launch tests
  pytest

  # to launch devserver
  python headless/wsgi.py
